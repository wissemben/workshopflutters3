import 'package:flutter/material.dart';

class Signin extends StatelessWidget {
  const Signin({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SignIn"),
      ),
      body: ListView(children: [
        Container(
            width: double.infinity,
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: Image.asset("assets/images/minecraft.jpg",
                width: 460, height: 215)),
        const SizedBox(
          height: 50,
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: const TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: "Username"),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: const TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), labelText: "Mot de passe"),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.blue, // background
            ),
            child: const Text("S'authentifier"),
            onPressed: () {},
          ),
        ),
      Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child:ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.red, // background
          ),
          child: const Text("Créer un compte"),
          onPressed: () {},
        ),
      ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            SizedBox(
              height: 50,
            ),
            Text("Mot de passe oublié ?  "),
            Center(
              child: Text("Cliquez ici",
                  style: TextStyle(color: Colors.blueAccent),
                  textDirection: TextDirection.ltr),
            ),
          ],
        )
      ]),
    );
  }
}
